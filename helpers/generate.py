
import argparse
import sys
parser = argparse.ArgumentParser(description='Auto FB login')
parser.add_argument('--username', default=None, required=True, help='FB username')
parser.add_argument('--password', default=None, required=True, help='FB password')
try:
    options = parser.parse_args()
except:
    parser.print_help()
    sys.exit(0)